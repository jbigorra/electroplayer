module.exports = {
  // Put your normal webpack config below here
  module: {
    rules: [
      ...require("./webpack.rules"),
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  }
};
