import React from "react";
import ReactDOM from "react-dom";

import App from "./UI/App.jsx";

ReactDOM.render(<App />, document.getElementById("electroplayer"));
